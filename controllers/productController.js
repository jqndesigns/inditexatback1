const { sendResponse } = require("../helpers/fetcher");
const chalk = require("chalk");
const items = require("../mockAPI/productList");

/**
 * Return a list of all products
 */
const getAllproducts = async (req, res) => {
  console.log(chalk.blue("Requesting all products"));
  const body = [...items];

  setTimeout(() => {
    //This API has a random second delay to simulate a real API connection to a server
    sendResponse(body, res);
  }, Math.random() * 1000);
};

/**
 * Return the detail of a specific product
 */
const getProductDetail = async (req, res) => {
  const productId = req.params.id;
  console.log(chalk.blue(`Requesting product ${productId} detail`));

  const item = items.find((i) => i.id === +productId);

  if (!item) {
    item = {
      err: "Item not found",
      status: 404,
    };

    console.log(chalk.red(item.err));
  }

  setTimeout(() => {
    //This API has a random second delay to simulate a real API connection to a server
    sendResponse(item, res);
  }, Math.random() * 1000);
};

const addProductToCart = async (req, res) => {
  console.log(chalk.blue("Saving a product to clients cart"));
  const productId = req.body.id;
  const colorId = req.body.color;
  const memoryId = req.body.memory;

  let response = {};

  if (req.body) {
    response = {
      response: "OK",
      productId,
      colorId,
      memoryId,
    };
  } else {
    response = {
      err: "Error while trying to add product to cart",
    };
  }

  setTimeout(() => {
    //This API has a random second delay to simulate a real API connection to a server
    response.err
      ? console.log(chalk.red(response.err))
      : console.log(chalk.green(`Product ${productId} added to cart`));
    sendResponse(response, res);
  }, Math.random() * 1000);
};

module.exports = {
  getAllproducts,
  getProductDetail,
  addProductToCart,
};
