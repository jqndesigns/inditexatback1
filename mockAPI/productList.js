const items = [
  {
    brand: "DOOGEE",
    colors: [
      {
        colorId: 1,
        colorName: "Red",
      },
      {
        colorId: 2,
        colorName: "Green",
      },
      {
        colorId: 3,
        colorName: "Blue",
      },
    ],
    id: 1,
    img: "https://m.media-amazon.com/images/I/71VTKu+enbL._AC_SL1500_.jpg",
    memory: [
      {
        memoryCapacity: 32,
        memoryId: 1,
      },
      {
        memoryCapacity: 64,
        memoryId: 2,
      },
      {
        memoryCapacity: 128,
        memoryId: 3,
      },
    ],
    model: "S41",
    price: 118.99,
    specs: {
      battery: "6300 mA",
      cam: "Cámara Triple A",
      cpu: "No especificado",
      dimension: "16.25x7.78x1.62 cm",
      ram: "6 GB",
      resolution: "720 x 1440",
      so: "Android 12.0",
      weight: "262 gr",
    },
  },
  {
    brand: "OPPO",
    id: 2,
    img: "https://m.media-amazon.com/images/I/61blcKk36+L._AC_SL1200_.jpg",
    memory: [
      {
        memoryCapacity: 32,
        memoryId: 1,
      },
      {
        memoryCapacity: 64,
        memoryId: 2,
      },
      {
        memoryCapacity: 128,
        memoryId: 3,
      },
    ],
    model: "A57S",
    price: 178,
    specs: {
      battery: "5000 mA",
      cam: "Frontal 8mp",
      cpu: "No especificado",
      dimension: "16.37x7.5x0.8 cm",
      ram: "4 GB",
      resolution: "1612 x 720",
      so: "Android 12.0",
      weight: "189 gr",
    },
  },
  {
    brand: "SAMSUNG",
    colors: [
      {
        colorId: 1,
        colorName: "Red",
      },
      {
        colorId: 2,
        colorName: "Green",
      },
      {
        colorId: 3,
        colorName: "Blue",
      },
    ],
    id: 3,
    img: "https://m.media-amazon.com/images/I/71GKjApyuKL._AC_SL1500_.jpg",
    model: "M13",
    price: 147.05,
    specs: {
      battery: "5000 mA",
      cam: "Principal 50mp",
      cpu: "No especificado",
      dimension: "7.69x0.84x16.54 cm ",
      ram: "64 GB",
      resolution: "No especificado",
      so: "Android 12.0",
      weight: "192 gr",
    },
  },
  {
    brand: "Xiaomi",
    colors: [
      {
        colorId: 1,
        colorName: "Red",
      },
      {
        colorId: 2,
        colorName: "Green",
      },
      {
        colorId: 3,
        colorName: "Blue",
      },
    ],
    id: 4,
    img: "https://m.media-amazon.com/images/I/51YxFQpw+QL._AC_SL1001_.jpg",
    memory: [
      {
        memoryCapacity: 32,
        memoryId: 1,
      },
      {
        memoryCapacity: 64,
        memoryId: 2,
      },
      {
        memoryCapacity: 128,
        memoryId: 3,
      },
    ],
    model: "Note 11S",
    price: 196.41,
    specs: {
      battery: "5000 mA",
      cam: "Frontal 108mp",
      cpu: "No especificado",
      dimension: " 7.38x0.81x15.98 cm",
      ram: "8 GB",
      resolution: "No especificado",
      so: "Android 10.0",
      weight: "179 gr",
    },
  },
  {
    brand: "Blackview",
    colors: [
      {
        colorId: 1,
        colorName: "Red",
      },
      {
        colorId: 2,
        colorName: "Green",
      },
      {
        colorId: 3,
        colorName: "Blue",
      },
    ],
    id: 5,
    img: "https://m.media-amazon.com/images/I/61seOq5YCsL._AC_SL1500_.jpg",
    model: "BV8800",
    price: 280.49,
    specs: {
      battery: "8380 mA",
      cam: "Frontal 20mp",
      cpu: "No especificado",
      dimension: "23.1x20.1x4.1 cm mm",
      ram: "8 GB",
      resolution: "1920x1080",
      so: "Android",
      weight: "700 gr",
    },
  },
  {
    brand: "SAMSUNG",
    colors: [
      {
        colorId: 1,
        colorName: "Red",
      },
      {
        colorId: 2,
        colorName: "Green",
      },
      {
        colorId: 3,
        colorName: "Blue",
      },
    ],
    id: 6,
    img: "https://m.media-amazon.com/images/I/71UrubBb1fL._AC_SL1500_.jpg",
    memory: [
      {
        memoryCapacity: 32,
        memoryId: 1,
      },
      {
        memoryCapacity: 64,
        memoryId: 2,
      },
      {
        memoryCapacity: 128,
        memoryId: 3,
      },
    ],
    model: "M23 5G",
    price: 209,
    specs: {
      battery: "5000 mA",
      cam: "Principal 50mp",
      cpu: "No especificado",
      dimension: "0.84x7.7x16.55 mm",
      ram: "4 GB",
      resolution: "No especificado",
      so: "Android 12.0",
      weight: "198 gr",
    },
  },
  {
    brand: "ONEPLUS",
    id: 7,
    img: "https://m.media-amazon.com/images/I/51+NfNMRysL._AC_.jpg",
    memory: [
      {
        memoryCapacity: 32,
        memoryId: 1,
      },
      {
        memoryCapacity: 64,
        memoryId: 2,
      },
      {
        memoryCapacity: 128,
        memoryId: 3,
      },
    ],
    model: "OnePlus 11 5G",
    price: 849,
    specs: {
      battery: "5000 mA",
      cam: "Principal 50mp",
      cpu: "No especificado",
      dimension: "18.3x9.6x6.3 cm",
      ram: "8 GB",
      resolution: "No especificado",
      so: "Android 13.0",
      weight: "610 gr",
    },
  },
];

module.exports = items;
